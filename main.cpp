#include "parser.h"
#include <iostream>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>
#include <llvm/Support/raw_ostream.h>

int main() {
    llvm::LLVMContext C;
    llvm::Module m("module", C);
    auto f = m.getOrInsertFunction("myfn", llvm::Type::getDoubleTy(C));
    f->print(llvm::errs());
    return 0;
}
