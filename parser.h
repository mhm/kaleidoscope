#include <string>
#include <tao/pegtl.hpp>
#include <tao/pegtl/contrib/parse_tree.hpp>
#include <type_traits>

namespace grammar {
using namespace tao::pegtl;
struct S0 : space {};
struct S : star<S0> {};
struct Name : identifier {};

struct KDef : TAO_PEGTL_KEYWORD("def") {};
struct KExtern : TAO_PEGTL_KEYWORD("extern") {};

struct Def : if_must<KDef, S, Name, one<'('>, S, one<')'>> {};
struct Extern : if_must<KExtern, S, Name, one<'('>, S, one<')'>> {};
struct Program : sor<Def, Extern> {};

/************************************************/

template <typename> struct Store : std::false_type {};

template <> struct Store<Name> : std::true_type {};

template <> struct Store<Def> : parse_tree::remove_content {};
template <> struct Store<Extern> : parse_tree::remove_content {};

/**************************************************/
void printNode(const parse_tree::node &n, const std::string &s = "") {
    // detect the root node:
    if (n.is_root()) {
        std::cout << "ROOT" << std::endl;
    } else {
        if (n.has_content()) {
            std::cout << s << n.name() << " \"" << n.content() << "\" at "
                      << n.begin() << " to " << n.end() << std::endl;
        } else {
            std::cout << s << n.name() << " at " << n.begin() << std::endl;
        }
    }
    // print all child nodes
    if (!n.children.empty()) {
        const auto s2 = s + "  ";
        for (auto &up : n.children) {
            printNode(*up, s2);
        }
    }
}


}
